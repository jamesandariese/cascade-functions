with builtins;
let fn = y: vs:
  if vs == [] then null else
  if head vs != null then head vs else
  y y (tail vs);
in
  fn fn
