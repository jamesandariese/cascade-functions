# `cascade-functions`

General purpose functions that might be useful.

One file per function.  Start with a _ to disable the function being automatically imported.

### Usage

```
with import (fetchGit "https://gitlab.com/jamesandariese/cascade-functions");
```
