with builtins;

p:
  let
    contents = readFile p;
    splitRes = split "\r?\n" contents;
    lines = filter isString splitRes;
  in lines
