with import (toString ./.);
with builtins;

elts: name: join-string ":" (take elts (match-all-flat "(..)" (hashString "sha256" name)))
