with builtins;

let join' = join': sep: sl:
      if sl == []
        then ""
        else if length sl == 1
          then head sl
	  else "${head sl}${sep}${join' join' sep (tail sl)}";
    join = join' join';
in

join
