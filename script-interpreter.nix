with builtins;

let file-lines = import ./file-lines.nix; in

f: ( let n = head (split-filename f); in
    let
      lines = file-lines f;
      firstLine = head lines;
      shebangMatch = match ''#! *(.*[/])?(([\][ ]|[^ /])*)( (.*))?'' firstLine;
    in
      if shebangMatch == null
        then rec { interpreter = "sh"; args = "";}
        else rec { interpreter = (elemAt shebangMatch 1); args = (elemAt shebangMatch 4);}
  )
