with builtins;

f:
  let mr = match "(.*)[.]([^.]*)|(.*)" f;
  in if head mr == null
    then { name = (elemAt mr 2); extension = null; }
    else { name = (elemAt mr 0); extension = (elemAt mr 1); }
