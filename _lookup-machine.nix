{enrich ? true, interfaceDefinition ? false, ...}@filterSpec:

with builtins;
let

attrList = s: attrValues (mapAttrs (k: v: {name = k; value = v;}) s);
combineAttrs = l: listToAttrs (concatMap attrList l);

t = fromTOML (readFile ../machines.toml);
machines = attrValues (mapAttrs (name: v: {name = name;} // v) t);
filterNullFilters = f: attrValues f != [ null ];
nonNullFilterSpecs = filter filterNullFilters (attrValues (mapAttrs (k: v: {"${k}" = v;}) filterSpec));
filteredFilterSpec = combineAttrs nonNullFilterSpecs;
matchAttrs = fAS: tAS: (all (k: fAS."${k}" == tAS."${k}") (attrNames (intersectAttrs fAS tAS)));
matchingMachines = filter (m: matchAttrs filteredFilterSpec m) machines;

in

if length matchingMachines > 1 then throw "lookup-machine found too many machines" else
if length matchingMachines < 1 then throw "lookup-machine did not find any machines" else

let machine = head matchingMachines; in
let machine = if enrich then import ./enrich-machine-attrs.nix machine else machine; in
machine
