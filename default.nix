let _ = builtins.trace "${toString ./.}/default.nix"; in

let import-folder = import (toString ./import-folder.nix);
in
  import-folder {path = (toString ./.);}
