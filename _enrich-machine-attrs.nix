with builtins;

machine:

let addressParts = match "([0-9.]*)/([0-9]*)" machine.address;
in

if addressParts == null then throw "could not split address into IP and subnet" else

let prefix = fromJSON (elemAt addressParts 1);
    ip = head addressParts;
in

machine // {
  inherit ip prefix;
  interface = {
    ipv4.addresses = [{
      address = ip;
      prefixLength = prefix;
    }];
    macAddress = machine.mac;
  };
}
