with builtins;
let mkIsFilenameAMatch = {filenameMatch ? "[^_].*", filenameBadMatch ? null, ...}@opt:
      {name, type, ...}:
             if name == "default.nix" then false
        else if builtins.match filenameMatch name == null then false
        else if filenameBadMatch != null && builtins.match filenameBadMatch name != null then false
        else if ! elem type ["symlink" "regular"] then false
        else if builtins.match ".*[.]nix" name == null then false
        else true;
    readDirItems = import ./readDirItems.nix;
    extractName = fn: let m = match "(.*[/])?([a-zA-Z0-9-]+)[.]nix" fn; in if m == null then throw "${fn} does not seem to have a correct filename" else head (tail m);
    matches = {path, ...}@opt:
      let isFilenameAMatch = mkIsFilenameAMatch opt;
      in
        listToAttrs (map ({name,...}: let pname = name; in {name = "${extractName name}"; value = import "${path}/${name}";}) (filter isFilenameAMatch (readDirItems "${path}" )));
in
  matches
