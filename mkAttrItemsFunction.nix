with builtins;

kname: vname: aset: attrValues (mapAttrs (k: v: {"${kname}" = k; "${vname}" = v;}) aset)
