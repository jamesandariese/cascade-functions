with builtins;

let take' = take': n: l: 
      if n < 0 then throw "take requires a positive whole number or 0." else
      if n == 0 || l == []
        then []
	else [ (head l) ] ++ (take' take' (n - 1) (tail l));
    take = take' take';
in

take
