with import (toString ./.);

with builtins; pattern: s: flatten (match-all pattern s)
