with builtins;
let flatten-once = foldl' (x: y: x ++ (if isList y then y else [y])) []; # curried.  takes a list as final arg.
    flatten-many' = flatten-many': l:
      let flattened = flatten-once l;
      in
        if flattened == l
	  then l
	  else flatten-many' flatten-many' flattened;
    flatten-many = flatten-many' flatten-many';
in
flatten-many
